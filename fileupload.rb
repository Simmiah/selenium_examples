require 'selenium-webdriver'
require 'rspec/expectations'

include RSpec::Matchers

def setup
  @driver = Selenium::WebDriver.for :firefox
end

def teardown
  @driver.quit
end

def run
  setup
  yield
  teardown
end

run do
  filename = "file.txt"
  file = File.join(Dir.pwd, filename)

  @driver.get 'http://automationpractice.com/index.php?controller=contact'
  @driver.find_element(id: 'fileUpload').send_keys file
  @driver.find_element(id: 'submitMessage').click

  uploaded_file = @driver.find_element(id: 'uploaded-files').text
  expect(uploaded_file).to eql filename
end