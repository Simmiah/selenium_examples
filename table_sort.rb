require 'selenium-webdriver'
require 'rspec/expectations'
include RSpec::Matchers

def setup
  @driver = Selenium::WebDriver.for :firefox
end

def teardown
  @driver.quit
end

def run
  setup
  yield
  teardown
end

# check dues sort ascending
run do
  @driver.get 'http://the-internet.herokuapp.com/tables'
  @driver.find_element(css: '#table1 thead tr th:nth-of-type(4)').click
  dues = @driver.find_elements(css: '#table tbody tr td:nth-of-type(4)')
  due_values = dues.map { |due| due.text.delete('$').to_f }
  expect(due_values).to eql due_values.sort
end

#check dues sort descending
run do
  @driver.get 'http://the-internet.herokuapp.com/tables'
  @driver.find_element(css: '#table1 thead tr th:nth-of-type(4)').click
  dues = @driver.find_elements(css: '#table tbody tr td:nth-of-type(4)')
  due_values = dues.map { |due| due.text.delete('$').to_f }
  expect(due_values).to eql due_values.sort.reverse
end

# check email sort
run do
  @driver.get 'http://the-internet.herokuapp.com/tables'
  @driver.find_element(css: '#table1 thead tr th:nth-of-type(3)').click
  emails = @driver.find_elements(css: '#table1 tbody tr td:nth-of-type(3)')
  email_values = emails.map { |email| email.text }
  expect(email_values).to eql email_values.sort
end